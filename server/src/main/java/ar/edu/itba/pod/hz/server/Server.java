package ar.edu.itba.pod.hz.server;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Server {
    private static final String MAP_NAME = "census";
    private static Logger logger = LoggerFactory.getLogger(Server.class);

    public static void main(String[] args) {
        logger.info("clase09-ejer01 Server Starting ...");
        HazelcastInstance instance = Hazelcast.newHazelcastInstance();
        instance.getMap(MAP_NAME);

    }
}
