package ar.edu.itba.pod.hz.client;

import ar.edu.itba.pod.hz.client.reader.CensusReader;
import ar.edu.itba.pod.hz.mr.query4.Query4CombinerFactory;
import ar.edu.itba.pod.hz.mr.query4.Query4MapperFactory;
import ar.edu.itba.pod.hz.mr.query4.Query4ReducerFactory;
import ar.edu.itba.pod.hz.mr.query5.Query5MapperFactory;
import ar.edu.itba.pod.hz.mr.query5.Query5ReducerFactory;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.mapreduce.Job;
import com.hazelcast.mapreduce.JobCompletableFuture;
import com.hazelcast.mapreduce.JobTracker;
import com.hazelcast.mapreduce.KeyValueSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;

public class Query5Executor {

    private static Logger logger = LoggerFactory.getLogger(Query1Executor.class);

    private static final String MAP_NAME_1 = "census";
    private static final String MAP_NAME_2 = "census2";


    public static void execute(File inFile, File outFile) throws IOException, ExecutionException, InterruptedException {

        HazelcastInstance client = Initializer.getClient();
        IMap<Integer, String> iMap = client.getMap(MAP_NAME_1);
        iMap.destroy();
        logger.info("Query5: Reading File...");
        try {
            new CensusReader(inFile).readDepartment(iMap);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        logger.info("Finished Reading File...");

        KeyValueSource<Integer, String> source = KeyValueSource.fromMap(iMap);

        JobTracker tracker = client.getJobTracker("default");

        Job<Integer, String> job = tracker.newJob(source);

        logger.info("Starting Map-Reduce...");

        JobCompletableFuture<Map<String, Integer>> future = job
                .mapper(new Query4MapperFactory())
                .combiner(new Query4CombinerFactory())
                .reducer(new Query4ReducerFactory())
                .submit();

        logger.info("Finished Map-Reduce...");

        IMap<String, Integer> anotherIMap = client.getMap(MAP_NAME_2);
        anotherIMap.destroy();
        anotherIMap.putAll(future.get());
        KeyValueSource<String, Integer> secondSource = KeyValueSource.fromMap(anotherIMap);
        JobTracker secondTracker = client.getJobTracker("default");
        Job<String, Integer> secondJob = secondTracker.newJob(secondSource);

        logger.info("Starting Map-Reduce...");

        JobCompletableFuture<List<String>> farInTheFuture = secondJob
                .mapper(new Query5MapperFactory())
                .reducer(new Query5ReducerFactory())
                .submit(iterable -> {
                    List<String> ans = new LinkedList<>();

                    PriorityQueue<Map.Entry<Integer, List<String>>> queue = new PriorityQueue<>((x, y) -> x.getKey().compareTo(y.getKey()));
                    iterable.forEach(x -> {
                        queue.offer(x);
                    });
                    while (!queue.isEmpty()) {
                        Map.Entry<Integer, List<String>> entry = queue.poll();
                        if(entry.getValue().size() > 1){
                            String[] aux = entry.getValue().toArray(new String[0]);
                            ans.add(entry.getKey().toString());

                            for (int i = 0; i < aux.length - 1; i++) {
                                for (int j = i + 1; j < aux.length; j++) {
                                    ans.add(String.format("%s + %s", aux[i].split("  ")[0], aux[j].split("  ")[0]));
                                }
                            }
                            ans.add("");
                        }
                    }
                    return ans;
                });

        List<String> list = farInTheFuture.get();
        logger.info("Finished Map-Reduce...\nWriting output...");

        FileWriter fw = new FileWriter(outFile);

        for (String line : list) {
            try {
                fw.write(line + "\n");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        fw.close();

        logger.info("Query5 Finished");
    }
}
