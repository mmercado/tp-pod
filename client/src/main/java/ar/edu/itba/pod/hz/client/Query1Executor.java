package ar.edu.itba.pod.hz.client;
import ar.edu.itba.pod.hz.client.reader.CensusReader;
import ar.edu.itba.pod.hz.mr.query1.Query1CombinerFactory;
import ar.edu.itba.pod.hz.mr.query1.Query1MapperFactory;
import ar.edu.itba.pod.hz.mr.query1.Query1ReducerFactory;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.mapreduce.Job;
import com.hazelcast.mapreduce.JobCompletableFuture;
import com.hazelcast.mapreduce.JobTracker;
import com.hazelcast.mapreduce.KeyValueSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class Query1Executor {
    private static Logger logger = LoggerFactory.getLogger(Query1Executor.class);

    private static final String MAP_NAME = "census";

    public static void execute(File inFile, File outFile) throws IOException, ExecutionException, InterruptedException {

        HazelcastInstance client = Initializer.getClient();
        IMap<Integer, Integer> iMap = client.getMap(MAP_NAME);
        iMap.destroy();

        logger.info("Query1: Reading File...");
        try {
            new CensusReader(inFile).readAges(iMap);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        logger.info("Finished Reading File...");

        KeyValueSource<Integer, Integer> source = KeyValueSource.fromMap(iMap);

        JobTracker tracker = client.getJobTracker("default");

        Job<Integer, Integer> job = tracker.newJob(source);

        logger.info("Starting Map-Reduce...");

        JobCompletableFuture<Map<Integer, Integer>> future = job
                .mapper(new Query1MapperFactory())
                .combiner(new Query1CombinerFactory())
                .reducer(new Query1ReducerFactory())
                .submit();

        Map<Integer, Integer> ans = future.get();

        logger.info("Finished Map-Reduce...\nWriting output...");

        FileWriter fw = new FileWriter(outFile);

        ans.forEach((k, v) -> {
            try {
                fw.write(getRange(k) + v + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        fw.close();
        logger.info("Query1 Finished");
    }

    private static String getRange(int code){
        String s = "";
        switch (code){
            case 1:
                s = "0-14 = ";
                break;
            case 2:
                s = "15-64 = ";
                break;
            case 3:
                s = "65-? = ";
                break;
            default:
                logger.error("Unexpected code for Age range");
                break;
        }
        return s;
    }

}
