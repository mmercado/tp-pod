package ar.edu.itba.pod.hz.client;

import ar.edu.itba.pod.hz.client.reader.CensusReader;
import ar.edu.itba.pod.hz.mr.query3.Query3MapperFactory;
import ar.edu.itba.pod.hz.mr.query3.Query3ReducerFactory;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.mapreduce.Job;
import com.hazelcast.mapreduce.JobCompletableFuture;
import com.hazelcast.mapreduce.JobTracker;
import com.hazelcast.mapreduce.KeyValueSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.ExecutionException;

public class Query3Executor {

  private static Logger logger = LoggerFactory.getLogger(Query1Executor.class);

  private static final String MAP_NAME = "census";

  public static void execute(File inFile, File outFile) throws IOException, ExecutionException, InterruptedException {

    int N = 0;
    try {
      N = Integer.parseInt(System.getProperty("n"));
    } catch (NumberFormatException e){
      logger.error("n must be integer");
      return;
    }

    HazelcastInstance client = Initializer.getClient();
    IMap<Integer, String[]> iMap = client.getMap(MAP_NAME);
    iMap.destroy();
    logger.info("Query3: Reading File...");
    try {
      new CensusReader(inFile).readIlliteracyIndex(iMap);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    logger.info("Finished Reading File...");

    KeyValueSource<Integer, String[]> source = KeyValueSource.fromMap(iMap);

    JobTracker tracker = client.getJobTracker("default");

    Job<Integer, String[]> job = tracker.newJob(source);
      logger.info("Starting Map-Reduce...");

      JobCompletableFuture<PriorityQueue<Map.Entry<String, Double>>> future = job
              .mapper(new Query3MapperFactory())
              .reducer(new Query3ReducerFactory())
              .submit(iterable -> {
                  PriorityQueue<Map.Entry<String, Double>> pq = new PriorityQueue<>((o1, o2) -> o2.getValue().compareTo(o1.getValue()));
                  iterable.forEach(pq::offer);
                  return pq;
              });

      PriorityQueue<Map.Entry<String, Double>> ans = future.get();

      logger.info("Finished Map-Reduce...\nWriting output...");

      FileWriter fw = new FileWriter(outFile);
      for (int i = 0; i < N; i++) {
          Map.Entry<String, Double> each = ans.poll();
          fw.write(each.getKey().split("-")[1].trim() + " = " + String.format("%.2f", each.getValue()) + "\n");
      }
      fw.close();
      logger.info("Query3 Finished");

  }
}
