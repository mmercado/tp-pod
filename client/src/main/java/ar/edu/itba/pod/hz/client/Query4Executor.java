package ar.edu.itba.pod.hz.client;

import ar.edu.itba.pod.hz.client.reader.CensusReader;
import ar.edu.itba.pod.hz.mr.query4.Query4CombinerFactory;
import ar.edu.itba.pod.hz.mr.query4.Query4MapperFactory;
import ar.edu.itba.pod.hz.mr.query4.Query4ReducerFactory;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.mapreduce.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;

public class Query4Executor {
    private static Logger logger = LoggerFactory.getLogger(Query1Executor.class);

    private static final String MAP_NAME = "census";
    private static final String PATH = "dataset-10000.csv";

    public static void execute(File inFile, File outFile) throws IOException, ExecutionException, InterruptedException {

        HazelcastInstance client = Initializer.getClient();
        IMap<Integer, String> iMap = client.getMap(MAP_NAME);
        iMap.destroy();
        int TOPE;

        try {
            TOPE = Integer.parseInt(System.getProperty("tope"));
        } catch (NumberFormatException e){
            logger.error("TOPE must be integer");
            return;
        }

        String PROV = System.getProperty("prov");

        logger.info("Query4: Reading File...");
        try {
            new CensusReader(inFile).readDepartment(iMap, PROV);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        logger.info("Finished Reading File...");

        KeyValueSource<Integer, String> source = KeyValueSource.fromMap(iMap);

        JobTracker tracker = client.getJobTracker("default");

        Job<Integer, String> job = tracker.newJob(source);

        logger.info("Starting Map-Reduce...");

        JobCompletableFuture<PriorityQueue<Map.Entry<String, Integer>>> future = job
                .mapper(new Query4MapperFactory())
                .combiner(new Query4CombinerFactory())
                .reducer(new Query4ReducerFactory())
                .submit(iterable -> {
                    PriorityQueue<Map.Entry<String, Integer>> ans = new PriorityQueue<>((x, y) -> y.getValue().compareTo(x.getValue()));
                            iterable.forEach(x -> {
                                if (x.getValue() < TOPE) {
                                    ans.offer(x);
                                }
                            });
                            return ans;
                        }
                );
        PriorityQueue<Map.Entry<String, Integer>> ans = future.get();
        logger.info("Finished Map-Reduce...\nWriting output...");

        FileWriter fw = new FileWriter(outFile);

        while (!ans.isEmpty()) {
            try {
                Map.Entry<String, Integer> current = ans.poll();
                fw.write(current.getKey().trim() + " = " + current.getValue() + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        fw.close();

        logger.info("Query4 Finished");
    }
}
