package ar.edu.itba.pod.hz.client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import static java.lang.System.exit;

public class Main {
  private static Logger logger = LoggerFactory.getLogger(Main.class);

  public static void main(String[] args) {
    Integer query;
    try{
      query = Integer.parseInt(System.getProperty("query"));

      final File inFile = new File(System.getProperty("inPath"));
      final File outFile = new File(System.getProperty("outPath"));

      switch (query){
        case 1:
          Query1Executor.execute(inFile, outFile);
          break;
        case 2:
          Query2Executor.execute(inFile, outFile);
          break;
        case 3:
          Query3Executor.execute(inFile, outFile);
          break;
        case 4:
          Query4Executor.execute(inFile, outFile);
          break;
        case 5:
          Query5Executor.execute(inFile, outFile);
          break;
        default:
          logger.error("Query number must be in range 1 to 5");
          exit(1);
      }
    } catch(NumberFormatException e){
      logger.error("Query must be a number");
    } catch (InterruptedException e) {
      e.printStackTrace();
    } catch (ExecutionException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    exit(0);
  }
}
