package ar.edu.itba.pod.hz.client.reader;

import com.hazelcast.core.IMap;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Map;

public class CensusReader {
    private Reader in;
    private Iterable<CSVRecord> records;

    public CensusReader(File inFile) throws IOException {
        this.in = new FileReader(inFile);
        records = CSVFormat.EXCEL.withHeader().parse(in);
    }

    public void readAges(IMap<Integer, Integer> iMap) throws IOException {
        int counter = 0;
        for (CSVRecord record : records) {
            iMap.put(counter++, Integer.valueOf(record.get("edad")));
        }
    }

    public void readHouseType(IMap<Integer, Integer[]> iMap) {
        int counter = 0;
        for (CSVRecord record : records) {
            Integer[] value = {Integer.valueOf(record.get("hogarid")), Integer.valueOf(record.get("tipovivienda"))};
            iMap.put(counter++, value);
        }
    }

    public void readIlliteracyIndex(IMap<Integer, String[]> iMap) {
        int counter = 0;
        for (CSVRecord record : records) {
            iMap.put(counter, new String[]{record.get("nombreprov") + "-" + record.get("nombredepto"), record.get("alfabetismo")});
            counter++;
        }
    }


    public void readDepartment(IMap<Integer, String> iMap, String nombreprov) {
        int counter = 0;
        for (CSVRecord record : records) {
            String recordProv = record.get("nombreprov").trim();
            if (recordProv.equals(nombreprov)) {
                iMap.put(counter++, record.get("nombredepto").trim());
            }
        }

    }

    public void readDepartment(IMap<Integer, String> iMap) {
        int counter = 0;
        for (CSVRecord record : records) {
            iMap.put(counter++, String.format("%s (%s)", record.get("nombredepto").split("  ")[0], record.get("nombreprov").split("  ")[0]));
        }

    }
}
