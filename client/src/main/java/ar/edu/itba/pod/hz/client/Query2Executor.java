package ar.edu.itba.pod.hz.client;

import ar.edu.itba.pod.hz.client.reader.CensusReader;
import ar.edu.itba.pod.hz.mr.query2.Query2MapperFactory;
import ar.edu.itba.pod.hz.mr.query2.Query2ReducerFactory;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.mapreduce.Job;
import com.hazelcast.mapreduce.JobCompletableFuture;
import com.hazelcast.mapreduce.JobTracker;
import com.hazelcast.mapreduce.KeyValueSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class Query2Executor {
    private static Logger logger = LoggerFactory.getLogger(Query1Executor.class);

    private static final String MAP_NAME = "census";


    public static void execute(File inFile, File outFile) throws IOException, ExecutionException, InterruptedException {

        HazelcastInstance client = Initializer.getClient();
        IMap<Integer, Integer[]> iMap = client.getMap(MAP_NAME);
        iMap.destroy();
        logger.info("Query2: Reading File...");
        try {
            new CensusReader(inFile).readHouseType(iMap);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        logger.info("Finished Reading File...");

        KeyValueSource<Integer, Integer[]> source = KeyValueSource.fromMap(iMap);

        JobTracker tracker = client.getJobTracker("default");

        Job<Integer, Integer[]> job = tracker.newJob(source);

        logger.info("Starting Map-Reduce...");

        JobCompletableFuture<Map<Integer, Double>> future = job
                .mapper(new Query2MapperFactory())
                .reducer(new Query2ReducerFactory())
                .submit();

        Map<Integer, Double> ans = future.get();

        logger.info("Finished Map-Reduce...\nWriting output...");

        FileWriter fw = new FileWriter(outFile);

        ans.forEach((k, v) -> {
            try {
                fw.write(k + " = " + String.format("%.2f", v) + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        fw.close();
        logger.info("Query2 Finished");
    }
}
