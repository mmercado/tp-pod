package ar.edu.itba.pod.hz.mr.query1;

import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;

/**
 * Created by juan on 13/11/16.
 */
public class Query1MapperFactory implements Mapper<Integer, Integer, Integer, Integer> {
    @Override
    public void map(Integer key, Integer value, Context<Integer, Integer> context) {
        if (value < 15) {
            context.emit(1, 1);
        } else if (value < 65){
            context.emit(2, 1);
        }else{
            context.emit(3, 1);
        }
    }
}
