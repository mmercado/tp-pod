package ar.edu.itba.pod.hz.mr.query5;

import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;

/**
 * Created by juan on 16/11/16.
 */
public class Query5MapperFactory implements Mapper<String, Integer, Integer, String> {

    @Override
    public void map(String department, Integer population, Context<Integer, String> context) {
        context.emit(population - population % 100, department);
    }
}
