package ar.edu.itba.pod.hz.mr.query4;

import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;

/**
 * Created by juan on 13/11/16.
 */
public class Query4MapperFactory implements Mapper<Integer, String, String, Integer> {

    @Override
    public void map(Integer integer, String s, Context<String, Integer> context) {
        context.emit(s, 1);
    }
}
