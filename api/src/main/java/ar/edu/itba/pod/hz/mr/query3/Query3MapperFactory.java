package ar.edu.itba.pod.hz.mr.query3;

import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;

public class Query3MapperFactory implements Mapper<Integer, String[], String, Integer> {

  @Override
  public void map(Integer id, String[] values, Context<String, Integer> context) {
    context.emit(values[0], Integer.valueOf(values[1]));
  }
  
}
