package ar.edu.itba.pod.hz.mr.query1;

import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

public class Query1ReducerFactory implements ReducerFactory<Integer, Integer, Integer>{
    @Override
    public Reducer<Integer, Integer> newReducer(Integer key) {
        return new Reducer<Integer, Integer>() {
            int count;

            @Override
            public void reduce(Integer value) {
                count += value;
            }

            @Override
            public void beginReduce() {
                count  = 0;
            }

            @Override
            public Integer finalizeReduce() {
                return count;
            }
        };
    }
}
