package ar.edu.itba.pod.hz.mr.query2;

import com.hazelcast.mapreduce.Context;
import com.hazelcast.mapreduce.Mapper;

/**
 * Created by juan on 13/11/16.
 */
public class Query2MapperFactory implements Mapper<Integer, Integer[], Integer, Integer>{
    @Override
    public void map(Integer id, Integer[] values, Context<Integer, Integer> context) {
        context.emit(values[1], values[0]);
    }
}
