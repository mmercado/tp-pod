package ar.edu.itba.pod.hz.mr.query2;

import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by juan on 13/11/16.
 */
public class Query2ReducerFactory implements ReducerFactory<Integer, Integer, Double> {
    @Override
    public Reducer<Integer, Double> newReducer(Integer key) {
        return new Reducer<Integer, Double>() {
            Set<Integer> houseIds;
            int count;

            @Override
            public void reduce(Integer value) {
                houseIds.add(value);
                count++;
            }

            @Override
            public void beginReduce() {
                houseIds = new HashSet<>();
                count = 0;
            }

            @Override
            public Double finalizeReduce() {
                return count/(double)houseIds.size();
            }
        };
    }
}
