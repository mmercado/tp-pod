package ar.edu.itba.pod.hz.mr.query5;

import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by juan on 16/11/16.
 */
public class Query5ReducerFactory implements ReducerFactory<Integer, String, List<String>> {
    @Override
    public Reducer<String, List<String>> newReducer(Integer integer) {
        return new Reducer<String, List<String>>() {
            List<String> list;

            @Override
            public void beginReduce() {
                list = new LinkedList<>();
            }

            @Override
            public void reduce(String s) {
                list.add(s);
            }

            @Override
            public List<String> finalizeReduce() {
                return list;
            }
        };
    }
}
