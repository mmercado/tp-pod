package ar.edu.itba.pod.hz.mr.query4;

import com.hazelcast.mapreduce.Combiner;
import com.hazelcast.mapreduce.CombinerFactory;

/**
 * Created by juan on 13/11/16.
 */
public class Query4CombinerFactory implements CombinerFactory<String, Integer, Integer> {
    @Override
    public Combiner newCombiner(String s) {
        return new Combiner() {
            int count;

            @Override
            public void reset() {
                count = 0;
            }

            @Override
            public void beginCombine() {
                count = 0;
            }

            @Override
            public void combine(Object o) {
                count++;
            }

            @Override
            public Object finalizeChunk() {
                return count;
            }
        };
    }

}
