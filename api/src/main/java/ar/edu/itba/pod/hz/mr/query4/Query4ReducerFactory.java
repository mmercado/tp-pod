package ar.edu.itba.pod.hz.mr.query4;

import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

public class Query4ReducerFactory implements ReducerFactory<String, Integer, Integer>{
    @Override
    public Reducer<Integer, Integer> newReducer(String s) {
        return new Reducer<Integer, Integer>() {
            int acu;

            @Override
            public void beginReduce() {
                acu = 0;
            }

            @Override
            public void reduce(Integer integer) {
                acu += integer;
            }

            @Override
            public Integer finalizeReduce() {
                return acu;
            }
        };
    }
}
