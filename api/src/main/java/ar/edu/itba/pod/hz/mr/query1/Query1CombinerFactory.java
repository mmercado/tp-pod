package ar.edu.itba.pod.hz.mr.query1;

import com.hazelcast.mapreduce.Combiner;
import com.hazelcast.mapreduce.CombinerFactory;

/**
 * Created by juan on 13/11/16.
 */
public class Query1CombinerFactory implements CombinerFactory<Integer, Integer, Integer> {


    @Override
    public Combiner<Integer, Integer> newCombiner(Integer integer) {
        return new Combiner<Integer, Integer>() {
            int count = 0;

            @Override
            public void combine(Integer value) {
                count++;
            }

            @Override
            public Integer finalizeChunk() {
                return count;
            }

            @Override
            public void reset() {
                count = 0;
            }

        };
    }
}
