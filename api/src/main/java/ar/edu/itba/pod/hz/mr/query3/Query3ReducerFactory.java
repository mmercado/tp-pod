package ar.edu.itba.pod.hz.mr.query3;

import com.hazelcast.mapreduce.Reducer;
import com.hazelcast.mapreduce.ReducerFactory;

public class Query3ReducerFactory implements ReducerFactory<String, Integer, Double> {
  @Override
  public Reducer<Integer, Double> newReducer(String s) {
    return new Reducer<Integer, Double>() {
      private int count;
      private int illiterate;

      @Override
      public void beginReduce() {
        super.beginReduce();
        count = 0;
        illiterate = 0;

      }

      @Override
      public void reduce(Integer isLiterate) {
        if(isLiterate == 2){
          illiterate++;
        }
        count++;
      }

      @Override
      public Double finalizeReduce() {
        return illiterate/(double)count;
      }
    };
  }
}
